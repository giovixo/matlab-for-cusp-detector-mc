# MATLAB for Cusp #

### Contributors ###

* G. De Cesare - decesare.giovanni@gmail.com

### Infos ###

* Code for the analysis of the Cusp Monte Carlo data
* Dependences: MATLAB

### Directories ###

* matlab: MATLAB source code
* dataset: it contains a short sample data file

### How to run ###

* Open the MATLAB on your pc
* Eventually edit PAR.m file
* Eventually edit mc.m file
* Run mc.m

The code is based on two steps data analysis.

In step 1 the data file is read and the energy deposits list for each input photons is evaluated. We call these data "raw photon history"
or LEVEL 1 DATA for short (stored into the A array).

In step 2 the energy deposits are sampled in a pixelated detector. We call these data "pixelated photon history" ore LEVEL 2 DATA for
short (stored into Apix array).

The parameters of the run are stored into the PAR.m class.

### Pipeline example ("mc.m") ###

```
#!matlab
%% Step 1: read the file and return the data array
disp('> Step 1...');
fileName = '01PolCrabCusp.dat';
ticID = tic; [A, len] = mc_read(fileName, [190., 337.]); toc(ticID);
clear('ticID');
Ared = A(:,:,1:len);
clear('A');
disp('> The raw photons history Ared is ready.');
%% Analysis of the level 1 data (see 'help mc_tools1' for more)
figure;
mc_tools1(Ared, 1);
hold on;
mc_tools1(Ared, 2);
hold off;
%% Step 2: Convert A into a pixelated detector
disp('> Step 2...');
ticID = tic; Apix = mc_pixelize2(Ared); toc(ticID);
clear('ticID');
disp('> The pixelated photons history Apix is ready.');
%% Step 2b: apply energy resolution and threshold
Apix = mc_hit(Apix, PAR.THRESHOLD);
%% Analysis of the level 2 (pixelated detector) data (see 'help mc_tools2' for more)
figure;
mc_tools2(Apix, 2); % Evaluate the scattering map for double events
mc_tools2(Apix, 3); % Estimate the polarization factor Q
%% Analysis of the level 2 (pixelated detector) data (see 'help mc_polarization' for more)
figure;
load 'double_map.mat'
newMap = mc_map_filter(double_map, 2, 5);
[alpha, q]= mc_polarization(newMap, 3);
%dlmwrite('Q2alpha_e190-337_z_const_nofilter.txt', [alpha', q'],' ');
%dlmwrite('double_map_z_xxx_190-337.txt', double_map(80:120,80:120),',');
plot(alpha, q, 'r*')
axis([0 400 -0.8 0.8]);
xlabel('alpha (degree)','FontSize',18);
ylabel('Q','FontSize',18);
grid;


```

### Example of parameter file ("PAR.m")###
```
#!matlab
classdef PAR
    %PAR Some constant used for the data analysis
    %   Define some parameters, that can be used into the MATLAB functions
    properties( Constant )
        LOG = 0; % Log level (0: default, 1: verbose)
        DATA_PATH = '/Users/giovanni/Works/Cusp/MATLAB4Cusp/dataset/set02/';
        % The detector threshold (in MeV)
        THRESHOLD = 0.007;
        % => Standard detector
        % The detector size (mm)
        X_DET = 200.;
        Y_DET = 200.;
        Z_DET = 20.;
        % The voxel size (mm)
        X_VOXEL = 2.;
        Y_VOXEL = 2.;
        Z_VOXEL = 2.;
        % => Calliste-like detector
        % The detector size (mm)
        %X_DET = 200.;
        %Y_DET = 200.;
        %Z_DET = 20.;
        % The voxel size (mm)
        %X_VOXEL = 0.5;
        %Y_VOXEL = 0.5;
        %Z_VOXEL = 0.5;

        SAME_Z = true; % true if the double events have the same z
        FILTER = true; % Filter 2-5 on the "linear" Q estimation
        EVENTS_NUMBER = 500000; % The number of photons
        HISTORY_LENGTH = 20; % The recorded history for the pixelated detector
        POLARIZATION_ANGLES = [5:10:85 95:10:175 185:10:265 275:10:355];
        %POLARIZATION_ANGLES_MIN = [5:10:85 95:10:175 185:10:265 275:10:355]; % Used by 'mc_polarization(,3)'
        %POLARIZATION_ANGLES_MAX = [5:10:85 95:10:175 185:10:265 275:10:355] + 0.2;
        POLARIZATION_ANGLES_MIN = ([9:12:81 87:12:171 177:12:261 267:12:351]) - 1; % Used by 'mc_polarization(,3)'
        POLARIZATION_ANGLES_MAX = ([9:12:81 87:12:171 177:12:261 267:12:351]) + 1;
    end
end


```
