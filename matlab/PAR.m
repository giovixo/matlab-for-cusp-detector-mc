classdef PAR
    %PAR Some constant used for the data analysis 
    %   Define some parameters, that can be used into the MATLAB functions 
    properties( Constant )
        LOG = 0; % Log level (0: default, 1: verbose)
        DATA_PATH = '/Users/giovanni/Works/Cusp/MATLAB4Cusp/dataset/set02/';
        % The detector threshold (in MeV)
        THRESHOLD = 0.007;
        % => Standard detector
        % The detector size (mm)
        X_DET = 200.;
        Y_DET = 200.;
        Z_DET = 20.;
        % The voxel size (mm)
        X_VOXEL = 2.;
        Y_VOXEL = 2.;
        Z_VOXEL = 2.;
        % => Calliste-like detector
        % The detector size (mm)
        %X_DET = 200.;
        %Y_DET = 200.;
        %Z_DET = 20.;
        % The voxel size (mm)
        %X_VOXEL = 0.5;
        %Y_VOXEL = 0.5;
        %Z_VOXEL = 0.5;
        
        SAME_Z = true; % true if the double events have the same z
        FILTER = true; % Filter 2-5 on the "linear" Q estimation
        EVENTS_NUMBER = 500000; % The number of photons 
        HISTORY_LENGTH = 20; % The recorded history for the pixelated detector
        POLARIZATION_ANGLES = [5:10:85 95:10:175 185:10:265 275:10:355];
        %POLARIZATION_ANGLES_MIN = [5:10:85 95:10:175 185:10:265 275:10:355]; % Used by 'mc_polarization(,3)'
        %POLARIZATION_ANGLES_MAX = [5:10:85 95:10:175 185:10:265 275:10:355] + 0.2;
        POLARIZATION_ANGLES_MIN = ([9:12:81 87:12:171 177:12:261 267:12:351]) - 1; % Used by 'mc_polarization(,3)'
        POLARIZATION_ANGLES_MAX = ([9:12:81 87:12:171 177:12:261 267:12:351]) + 1;
    end
end

